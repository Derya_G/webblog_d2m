package de.awacademy.webblogD2M.kategorie;

import de.awacademy.webblogD2M.beitrag.BeitragDTO;

import java.util.List;

public class KategorieDTO {

    private String kategorieName;
    private List<BeitragDTO> beitraege;

    public String getKategorieName() {
        return kategorieName;
    }


    public void setKategorieName(String kategorieName) {
        this.kategorieName = kategorieName;
    }

    public List<BeitragDTO> getBeitraege() {
        return beitraege;
    }

    public void setBeitraege(List<BeitragDTO> beitraege) {
        this.beitraege = beitraege;
    }

}
