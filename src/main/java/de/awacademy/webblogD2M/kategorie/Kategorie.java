package de.awacademy.webblogD2M.kategorie;

import de.awacademy.webblogD2M.beitrag.Beitrag;

import javax.persistence.*;
import java.util.List;

@Entity
public class Kategorie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String kategorieName;

    @ManyToMany(mappedBy = "kategorien")
    private List<Beitrag> beitraege;

    public Kategorie() {
    }

    public Kategorie(String kategorieName) {
        this.kategorieName = kategorieName;
    }

    public String getKategorieName() {
        return kategorieName;
    }

    public List<Beitrag> getBeitraege() {
        return beitraege;
    }

    public void setKategorieName(String kategorieName) {
        this.kategorieName = kategorieName;
    }

    public void setBeitraege(List<Beitrag> beitraege) {
        this.beitraege = beitraege;
    }

    public void addBeitragToBeitraege(Beitrag beitrag){
        this.beitraege.add(beitrag);
    }

    public long getId() {
        return id;
    }
}
