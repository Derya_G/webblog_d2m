package de.awacademy.webblogD2M.beitrag;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BeitragRepository extends JpaRepository<Beitrag, Long> {
        List<Beitrag> findAllByOrderByErstellungsDatumDesc();
}
