package de.awacademy.webblogD2M.archivbeitrag;

import de.awacademy.webblogD2M.beitrag.Beitrag;
import de.awacademy.webblogD2M.user.User;

import javax.persistence.*;
import java.time.Instant;

@Entity
public class Archivbeitrag {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Lob
    private String text;

    private String titel;
    private Instant erstellungsDatum;

    @ManyToOne
    private User user;

    @ManyToOne
    private Beitrag beitrag;

    //Konstruktoren
    public Archivbeitrag() {
    }

    public Archivbeitrag(String text, String titel, Instant erstellungsDatum, User user, Beitrag beitrag) {
        this.text = text;
        this.titel = titel;
        this.erstellungsDatum = erstellungsDatum;
        this.user = user;
        this.beitrag = beitrag;
    }

    public Archivbeitrag(String text, User user, String titel, Instant erstellungsDatum) {
        this.text = text;
        this.user = user;
        this.titel = titel;
        this.erstellungsDatum = erstellungsDatum;
    }

    //Getter

    public String getText() {
        return text;
    }

    public String getTitel() {
        return titel;
    }

    public Instant getErstellungsDatum() {
        return erstellungsDatum;
    }

    public User getUser() {
        return user;
    }
}
