package de.awacademy.webblogD2M.kommentar;

import de.awacademy.webblogD2M.beitrag.Beitrag;
import de.awacademy.webblogD2M.user.User;

import javax.persistence.*;
import java.time.Instant;

@Entity
public class Kommentar {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Lob
    private String text;

    private Instant erstellungsDatum;

    @ManyToOne
    private User user;

    @ManyToOne
    private Beitrag beitrag;

    public Kommentar(){
    }

    public Kommentar(String text){
        this.text = text;
        this.erstellungsDatum = Instant.now();
    }

    //Getter / Setter
    public String getText() {
        return text;
    }

    public Instant getErstellungsDatum() {
        return erstellungsDatum;
    }

    public void setErstellungsDatum(Instant erstellungsDatum) {
        this.erstellungsDatum = erstellungsDatum;
    }

    public User getUser() {
        return user;
    }

    public long getId() {
        return id;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setBeitrag(Beitrag beitrag) {
        this.beitrag = beitrag;
    }
}
