package de.awacademy.webblogD2M.kommentar;

public class KommentarDTO {

    private String text = "";
    private String erstellungsDatum;

    public String getText() {
        return text;
    }

    public String getErstellungsDatum() {
        return erstellungsDatum;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setErstellungsDatum(String erstellungsDatum) {
        this.erstellungsDatum = erstellungsDatum;
    }
}
