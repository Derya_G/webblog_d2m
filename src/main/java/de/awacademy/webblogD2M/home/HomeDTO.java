package de.awacademy.webblogD2M.home;

import java.util.LinkedList;
import java.util.List;

public class HomeDTO {
    private List<Long> kategorieIdList = new LinkedList<>();
//    private String anzeige = "";

    public List<Long> getKategorieIdList() {
        return kategorieIdList;
    }

    public void setKategorieIdList(List<Long> kategorieIdList) {
        this.kategorieIdList = kategorieIdList;
    }
//
//    public String getAnzeige() {
//        return anzeige;
//    }
//
//    public void setAnzeige(String anzeige) {
//        this.anzeige = anzeige;
//    }
}
