package de.awacademy.webblogD2M.user;

import de.awacademy.webblogD2M.beitrag.Beitrag;
import de.awacademy.webblogD2M.kommentar.Kommentar;

import javax.persistence.*;
import java.util.List;


@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true)
    private String name;

    private boolean admin;
    private String passwort;

    @OneToMany(mappedBy = "user")
    private List<Kommentar> kommentare;

    @OneToMany(mappedBy = "user")
    private List<Beitrag> beitraege;

    public User() {
    }

    public User(String name, String passwort) {
        this.name = name;
        this.passwort = passwort;
    }

    //Getter
    public String getName() {
        return name;
    }

    public void addKommentarToKommentare(Kommentar kommentar){
        this.kommentare.add(kommentar);
    }
    public boolean isAdmin() {
        return admin;
    }

    public long getId() {
        return id;
    }



    //Setter
    public void setAdmin(boolean admin) {
        this.admin = admin;
    }
}

