package de.awacademy.webblogD2M.login;

import de.awacademy.webblogD2M.session.Session;
import de.awacademy.webblogD2M.session.SessionRepository;
import de.awacademy.webblogD2M.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

@Service
public class LoginService {

    @Autowired
    private SessionRepository sessionRepository;

    public void login(User user, HttpServletResponse response) {
        Session ses = new Session(user);
        sessionRepository.save(ses);

        Cookie cookie = new Cookie("sessionId", ses.getId());
        response.addCookie(cookie);
    }
}

