package de.awacademy.webblogD2M.signup;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class SignupDTO {

    @NotEmpty
    private String benutzername = "";

    @Size(min = 6, message = "Das Passwort muss mindestens sechs Zeichen lang sein.")
    private String passwort1 = "";
    private String passwort2 = "";

    public String getBenutzername() {
        return benutzername;
    }

    public void setBenutzername(String benutzername) {
        this.benutzername = benutzername;
    }

    public String getPasswort1() {
        return passwort1;
    }

    public void setPasswort1(String passwort1) {
        this.passwort1 = passwort1;
    }

    public String getPasswort2() {
        return passwort2;
    }

    public void setPasswort2(String passwort2) {
        this.passwort2 = passwort2;
    }
}

